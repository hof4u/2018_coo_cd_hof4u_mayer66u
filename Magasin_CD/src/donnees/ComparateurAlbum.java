package donnees;

public class ComparateurAlbum implements ComparateurCD{
	@Override
	public boolean etreAvant(CD c, CD d) {
		return (c.getNomAlbum().compareTo(d.getNomAlbum())<0);
	}
}
