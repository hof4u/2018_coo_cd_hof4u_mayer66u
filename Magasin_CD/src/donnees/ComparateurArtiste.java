package donnees;

public class ComparateurArtiste implements ComparateurCD{
	public boolean etreAvant(CD c, CD d) {
		return (c.getNomArtiste().compareTo(d.getNomArtiste())<0);
	}
}
