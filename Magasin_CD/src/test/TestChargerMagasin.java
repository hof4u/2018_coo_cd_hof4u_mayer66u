package test;

import java.io.FileNotFoundException;

import XML.ChargeurMagasin;
import static org.junit.Assert.*;
import org.junit.*;

public class TestChargerMagasin {
	
	@Test
	public void testRepertoireValide() throws FileNotFoundException {
		//initialisation des donn�es
		ChargeurMagasin cM = new ChargeurMagasin("musicbrainzSimple");
		int rep = cM.chargerMagasin().getNombreCds();
		assertEquals("Il n'y a pas le bon nombre de Cds charg�s", rep, 12);
	}
	
	
	@Test ( expected = FileNotFoundException.class )
	public void testRepertoireNonValide() throws Exception{
		new ChargeurMagasin("m").chargerMagasin();
	}
}
